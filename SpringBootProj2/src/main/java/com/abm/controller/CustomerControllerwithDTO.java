package com.abm.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abm.dto.CustomerDetailsDto;
import com.abm.dto.RegistrationStatus;
import com.abm.entity.Customer;
import com.abm.exceptions.CustomerServiceException;
import com.abm.service.CustomerService;

@RestController
public class CustomerControllerwithDTO {
	
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping("/register_with_dto")
	public RegistrationStatus registerCustomer(CustomerDetailsDto customerDetailsDto) {
		RegistrationStatus status = new RegistrationStatus();
		try {
			Customer customer = new Customer();
			BeanUtils.copyProperties(customerDetailsDto, customer);
			customer.setPassword(Base64.getEncoder().encodeToString(customerDetailsDto.getPassword1().getBytes())); 
			
			// to save uploaded file in a folder
			try {
				String fileName = customerDetailsDto.getProfilePic().getOriginalFilename();
				customer.setProfilePic(fileName);

				InputStream is  = customerDetailsDto.getProfilePic().getInputStream();
				FileOutputStream os = new FileOutputStream("C:/Users/shubham.patil/Desktop/Upload//"+fileName);
				FileCopyUtils.copy(is, os);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			
			customerService.register(customer);
			status.setStatus(true);
			status.setRegisteredCustomerId(customer.getId());
			status.setStatusMessage("Customer registered successfully...");
			return status;
			//return "Customer registered successfully...";
		}catch(CustomerServiceException exception){
			status.setStatus(false);
			status.setStatusMessage(exception.getMessage());
			//return exception.getMessage();
			return status;
		}	
	}
}
