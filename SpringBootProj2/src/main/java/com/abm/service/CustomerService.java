package com.abm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abm.entity.Customer;
import com.abm.exceptions.CustomerServiceException;
import com.abm.repository.CustomerRepository;

@Service
@Transactional
public class CustomerService {
	
	@Autowired
	private  CustomerRepository customerRepository;
	
	private boolean isCustomerPresent(String email) {
		return customerRepository.findByEmail(email) == 1 ? true : false ; 
	}
	
	public void register(Customer customer) throws CustomerServiceException {
		if(isCustomerPresent(customer.getEmail()))
			throw new CustomerServiceException("Customer already rigistered");
		else
			customerRepository.save(customer);
	}
	
}
