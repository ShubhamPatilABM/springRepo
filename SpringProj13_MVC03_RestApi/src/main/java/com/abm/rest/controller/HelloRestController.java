package com.abm.rest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloRestController {
	
	@RequestMapping("/helloAgain")
	public String sayHello(String name) {
		return "Wellcome " +name+ " to Restfull Services...";
		// OP comes on appending :: ?name=SP
		             // :: >> http://localhost:8081/SpringProj13_MVC03_RestApi/helloAgain?name=SP
	}

}
