package com.abm.rest.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abm.entity.Passenger;
import com.abm.entity.Pnr;
import com.abm.enums.Gender;
import com.abm.enums.Status;

//assumption: working for IRCTC
@RestController
public class PNRController {
	
	@RequestMapping("/pnr-status")
	public Pnr checkPnrStatus(@RequestParam("pnrNo") int pnrNo) {
		Pnr pnr = new Pnr();
		pnr.setPnrNo(pnrNo);
		pnr.setTrainNo(1234);
		pnr.setTravelDate(LocalDate.of(2022, 11, 16));
		
		List<Passenger> passengers = new ArrayList<>();
		
		Passenger p1 = new Passenger();
		p1.setName("Shyam");
		p1.setGender(Gender.MALE);
		p1.setStatus(Status.CONFIRMED);

		Passenger p2 = new Passenger();
		p2.setName("Ram");
		p2.setGender(Gender.MALE);
		p2.setStatus(Status.WAITING);

		Passenger p3 = new Passenger();
		p3.setName("Preeti");
		p3.setGender(Gender.FEMALE);
		p3.setStatus(Status.CONFIRMED);
		
		passengers.add(p1);
		passengers.add(p2);
		passengers.add(p3);
		
		pnr.setPassengers(passengers);
		
		return pnr;
	}

}
