package com.abm.enums;

public enum Gender {
	MALE, FEMALE, OTHER;
}
