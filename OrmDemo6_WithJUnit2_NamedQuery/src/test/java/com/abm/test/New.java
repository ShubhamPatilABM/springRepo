package com.abm.test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.junit.Test;

import om.abm.dao.AlbumSongDao;
import om.abm.entity.one_to_many.Album;
import om.abm.entity.one_to_many.Song;

public class New {
	
	
	@Test
	public void addAlbum() {
		Album album = new Album();
		album.setaName("Hits of 2022");
		album.setReleaseDate(LocalDate.of(2022, 11, 11));
		album.setCopyRights("T-series");

		AlbumSongDao dao = new AlbumSongDao();
		dao.addAlbum(album);
	}

	@Test
	public void addSongToAlbum() {
		AlbumSongDao dao = new AlbumSongDao();

		// fetch album for which song to be added
		Album album = dao.fetchAlbum(2);

		Song s = new Song();
		s.setsName("Ja.. too...");
		s.setArtist("RPN");
		s.setDuration(LocalTime.of(0, 3, 22));

		s.setAlbum(album);

		dao.addSong(s);
	}

	@Test
	public void fetchSongsByArtist() {
		AlbumSongDao dao = new AlbumSongDao();
		List<Song> songs = dao.fetchSongsByArtist("RPN");
		for (Song song : songs) {
			System.out.println(song.getsName());
			System.out.println(song.getDuration());

			// CAUTION :: NOT RECOMMENDED TO USE println in jUnit
		}

	}

	@Test
	public void fetchAlbumsByArtist() {
		AlbumSongDao dao = new AlbumSongDao();
		List<Album> albums = dao.fetchAlbumsByArtist("RPN");
		for (Album album : albums) {
			System.out.println(album.getaId());
			System.out.println(album.getaName());
			System.out.println(album.getReleaseDate());

			// CAUTION :: NOT RECOMMENDED TO USE println in jUnit
		}

	}

	@Test
	public void fetchSongsByArtistForYear() {
		AlbumSongDao dao = new AlbumSongDao();
		List<Song> songs = dao.fetchSongsByArtistForYear("RPN", 2021);
		for (Song song : songs) {
			System.out.println(song.getsName());
			System.out.println(song.getDuration());

			// CAUTION :: NOT RECOMMENDED TO USE println in jUnit
		}

	}
}
