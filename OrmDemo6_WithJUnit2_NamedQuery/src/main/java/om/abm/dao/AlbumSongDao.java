package om.abm.dao;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import om.abm.entity.one_to_many.Album;
import om.abm.entity.one_to_many.Song;

public class AlbumSongDao {

	Album album = null;
	Song song = null;
	
	
	

	public void addSong(Song song) {
		// read data from persistence.xml
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin(); // transaction began
		em.persist(song); // persist() method generates insert query
		tx.commit();
		em.close();
		emf.close();
	}

	public void addAlbum(Album album) {
		// read data from persistence.xml
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin(); // transaction began
		em.persist(album); // persist() method generates insert query
		tx.commit();
		em.close();
		emf.close();
	}
	
	public Album fetchAlbum(int id) {
		// read data from persistence.xml
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");
		EntityManager em = emf.createEntityManager();
		album = em.find(Album.class, id);
		em.close();
		emf.close();
		return album;
	}	
	
	public Song fetchSong(int id) {
		// read data from persistence.xml
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");
		EntityManager em = emf.createEntityManager();
		song = em.find(Song.class, id);
		em.close();
		emf.close();
		return song;
	}
	
	public List<Song> fetchSongsByArtist(String artist){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");
		EntityManager em = emf.createEntityManager();
		
		//Query q = em.createQuery(" select s from Song s where s.artist =: artist ");  // HB or JP query ===> Customer ~ Entity class
		
		// >>> advantages of createNamedQuery eliminates HQL/JPQL query to SQL translation time in method call
		// it gets pre-translated query from Entity class where it gets translated on loading Entity class 
		// createNamedQuery gives high performance than createQuery
		Query q = em.createNamedQuery("songsByArtist");
		q.setParameter("artist", artist);
		List<Song> songlist =  q.getResultList();
		
		em.close();
		emf.close();
		return songlist;		

	}
	
	public List<Album> fetchAlbumsByArtist(String artist){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");
		EntityManager em = emf.createEntityManager();
		
		//select DISTINCT  a.* from tbl_album1 a join tbl_song1 s on  a.al_id = s.al_id where song_artist='RPN';
		Query q = em.createQuery(" select distinct a from Album a join a.song  s where s.artist =: artist ");  // HB or JP query ===> Customer ~ Entity class
		q.setParameter("artist", artist);
		List<Album> albumlist =  q.getResultList();
		
		em.close();
		emf.close();
		return albumlist;		

	}
	
	public List<Song> fetchSongsByArtistForYear(String artist, int releaseDate){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");
		EntityManager em = emf.createEntityManager();
		
		//select s.* from tbl_song1 s join tbl_album1 a on  a.al_id = s.al_id where song_artist='RPN' and Year(a.AL_RELEASE_DATE )=2021;
		Query q = em.createQuery(" select distinct s from Song s join s.album a where s.artist =: artist and year(a.releaseDate) =:releaseDate ");  // HB or JP query ===> Customer ~ Entity class
		q.setParameter("artist", artist);
		q.setParameter("releaseDate", releaseDate);
		List<Song> songlist =  q.getResultList();
		
		em.close();
		emf.close();
		return songlist;		

	}

	
}
