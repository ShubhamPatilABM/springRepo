package om.abm.entity.one_to_many;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_album1")
public class Album {
	
	@Id //PK
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column(name = "al_id")
	private int aId;
	
	@Column(name = "al_name")
	private String aName;
	
	@Column(name = "al_release_date")
	private LocalDate releaseDate;
	
	@Column(name = "al_copyrights")
	private String copyRights;
	
	// for @OneToMany >> default is  FetchType.LAZY
	// for @ManyToOne >> default is  FetchType.EAGER
	@OneToMany(mappedBy = "album", fetch = FetchType.EAGER)  // an album have many songs 
	private List<Song> song;

	public int getaId() {
		return aId;
	}

	public void setaId(int aId) {
		this.aId = aId;
	}

	public String getaName() {
		return aName;
	}

	public void setaName(String aName) {
		this.aName = aName;
	}

	public LocalDate getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getCopyRights() {
		return copyRights;
	}

	public void setCopyRights(String copyRights) {
		this.copyRights = copyRights;
	}

	public List<Song> getSong() {
		return song;
	}

	public void setSong(List<Song> song) {
		this.song = song;
	}
	
}
