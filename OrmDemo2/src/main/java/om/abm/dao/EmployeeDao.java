package om.abm.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import om.abm.entity.uni_association.Employee;

public class EmployeeDao {

	// read data from persistence.xml
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");
	EntityManager em = emf.createEntityManager();
	Employee employee = null;

	public void store(Employee employee) {
		EntityTransaction tx = em.getTransaction();
		tx.begin(); // transaction began
		em.persist(employee); // persist() method generates insert query
		tx.commit();
		em.close();
		emf.close();
	}

	public Employee fetch(int id) {
		employee = em.find(Employee.class, id);
		em.close();
		emf.close();
		return employee;
	}

	/*
	 * 
	 * 
	 * public List<Customer> fetchAll() { // select * from tblName Query q =
	 * em.createQuery(" select c from Customer c "); // HB or JP query ===> Customer
	 * ~ Entity class List<Customer> list = q.getResultList(); em.close();
	 * emf.close(); return list; }
	 * 
	 * public Customer fetch(String email) { Query q =
	 * em.createQuery(" select c from Customer c where c.email =: em "); // HB or JP
	 * query ===> Customer ~ Entity class q.setParameter("em", email); customer =
	 * (Customer) q.getSingleResult(); em.close(); emf.close(); return customer; }
	 * 
	 * public List<Customer> fetchByMonth(int month) { Query q = em.
	 * createQuery(" SELECT c FROM  Customer c WHERE MONTH(c.dateOfBirth) =: mon ");
	 * q.setParameter("mon", month); List<Customer> list = q.getResultList();
	 * em.close(); emf.close(); return list;
	 * 
	 * }
	 */

}
