package om.abm.entity.uni_association;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_loc")
public class Address {
	
	@Id //PK
	@GeneratedValue  
	@Column(name = "loc_id")
	private int lid;

	@Column(name = "loc_city")
	private String city;
	
	@Column(name = "loc_state")
	private String state;
	
	@Column(name = "pin_code")
	private int pinCode;

	public int getLid() {
		return lid;
	}

	public void setLid(int lid) {
		this.lid = lid;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getPinCode() {
		return pinCode;
	}

	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}

	@Override
	public String toString() {
		return "Address [lid=" + lid + ", city=" + city + ", state=" + state + ", pinCode=" + pinCode + "]";
	}

}
