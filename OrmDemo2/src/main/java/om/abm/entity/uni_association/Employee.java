package om.abm.entity.uni_association;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_emp")
public class Employee {

	@Id //PK
	@GeneratedValue 
	@Column(name = "emp_id")
	private int eid;
	
	@Column(name = "emp_name")
	private String name;
	
	@Column(name = "emp_salary")
	private double salary;

	// Uni-directional Association
	@OneToOne(cascade = CascadeType.ALL)   // one to one Association or Has-A-Relation
	@JoinColumn(name = "addr_id")  // FK 
	private Address address;

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Employee [eid=" + eid + ", name=" + name + ", salary=" + salary + "]";
	}
	
	

}
