package com.abm.app;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import javax.persistence.CascadeType;
import javax.persistence.OneToOne;

import om.abm.dao.AddressDao;
import om.abm.dao.EmployeeDao;
import om.abm.entity.uni_association.Address;
import om.abm.entity.uni_association.Employee;

public class App {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		String name = null;
		Double salary = null;
		String city = null;
		String state = null;
		Integer pinCode = null;

		System.out.println("*** Select Operation ***");
		int choice = 0;
		System.out.println("1. Insert Record"
		/*
		 * + "\n" + "2. Fetch details by ID"+"\n"+ "3. Fetch all records"+"\n"+
		 * "4. Fetch details by EmailId"+"\n"+ "5. Fetch details by Month"
		 */
		);
		choice = Integer.parseInt(s.nextLine());

		Employee employee = new Employee();
		Address address = new Address();

		EmployeeDao ed = new EmployeeDao();
		AddressDao ad = new AddressDao();

		switch (choice) {
		case 1:

			System.out.println("Enter Name");
			name = s.nextLine();
			employee.setName(name);
			System.out.println("Enter Salary");
			salary = Double.parseDouble(s.nextLine());
			employee.setSalary(salary);

			System.out.println("Enter City");
			city = s.nextLine();
			address.setCity(city);
			System.out.println("Enter state");
			state = s.nextLine();
			address.setState(state);
			System.out.println("Enter Pincode");
			pinCode = Integer.parseInt(s.nextLine());
			address.setPinCode(pinCode);
			
			employee.setAddress(address);
			/*
			 *Exception: Caused by: org.hibernate.TransientPropertyValueException: object references
			 * an unsaved transient instance - save the transient instance before flushing :
			 * om.abm.entity.Employee.address -> om.abm.entity.Address
			 * 
			 * ==> To avoid above Exception: In Entity class::>
			 * 			>>>>	 @OneToOne(cascade = CascadeType.ALL) 
			 * 
			 */
			// store data
			ed.store(employee);
			//ad.store(address);
			System.out.println("Record Inserted successfuly...");
			break;

		/*
		 * case 2: System.out.println("Enter Customer ID : "); int id =
		 * Integer.parseInt(s.nextLine()); customer = dao.fetch(id);
		 * System.out.println(customer.getName());
		 * System.out.println(customer.getEmail());
		 * System.out.println(customer.getDateOfBirth()); break;
		 * 
		 * case 3: list = dao.fetchAll(); //list.forEach(det-> System.out.println(det));
		 * list.forEach(System.out::println); break;
		 * 
		 * case 4: System.out.println("Enter Email Id :"); email = s.nextLine();
		 * customer = dao.fetch(email); System.out.println(customer.getId());
		 * System.out.println(customer.getName());
		 * System.out.println(customer.getDateOfBirth()); break;
		 * 
		 * case 5: System.out.println("Enter Month of Birth :"); int mon = s.nextInt();
		 * list = dao.fetchByMonth(mon); list.forEach(det-> System.out.println(det));
		 * break;
		 */

		default:
			System.out.println("Invalid selection");
			break;
		}

	}

}
