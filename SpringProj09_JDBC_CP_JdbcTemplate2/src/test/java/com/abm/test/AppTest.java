package com.abm.test;

import java.util.List;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abm.dao.interfaces.ICarPartInventory;
import com.abm.entity.CarPart;

public class AppTest {

	@Test
	public void insertParts() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-webmvc.xml");
		ICarPartInventory cpInv = (ICarPartInventory) ctx.getBean("carPartsInv3");

		// entity or model class is not instantiated by using spring
		CarPart cp = new CarPart();
		cp.setPartName("Tubes");
		cp.setCarModel("i20");
		cp.setPrice(5000);
		cp.setQuantity(4);

		cpInv.addNewPArt(cp);
	}
	
	@Test
	public void getPartsList() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-webmvc.xml");
		ICarPartInventory cpInv = (ICarPartInventory) ctx.getBean("carPartsInv3");

		// entity or model class is not instantiated by using spring
		List<CarPart> list = cpInv.getAvailableParts();
		for(CarPart cp : list) {
			System.out.println(cp.getPartNo());
			System.out.println(cp.getPartName());
			System.out.println(cp.getCarModel());
			System.out.println(cp.getPrice());
			System.out.println(cp.getQuantity());
			System.out.println("*******************************************");
		}
	
	}

}
