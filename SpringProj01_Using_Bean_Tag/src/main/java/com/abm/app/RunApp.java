package com.abm.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abm.component.HelloWorld;
import com.abm.component.TimeTeller;

public class RunApp {

	public static void main(String[] args) {
		// loading the IOC container
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-webmvc.xml");
		
		// Accessing a bean
		for(int i=0; i<=5; i++) {
			HelloWorld hw = (HelloWorld) ctx.getBean("hello");
			HelloWorld hw1 = (HelloWorld) ctx.getBean("hello");
			System.out.println(hw.sayHello("Shubham"));
			System.out.println(hw1.sayHello("Shubham"));
		}
		
		TimeTeller tt = (TimeTeller) ctx.getBean("timer");
		System.out.println(tt.getTime());
	}

}
