package com.abm.component;

public class HelloWorld {
	
	
	public HelloWorld() {
		System.out.println("HelloWorld Constructor call = No. of Objects created by Spring");
	}

	public String sayHello(String name) {
		return "Hello Mr. " + name;
	}

}
