// Inner/Nested classes

package com.abm.demoLambda1;

//outer class   // 1st useCase
class OuterA {

	private int x = 10;

	// Non static inner class accessing pvt var of outer class
	class InnerB {
		void check() {
			System.out.println(x);
		}
	}

	// static inner class accessing pvt var of outer class
	static class InnerC {
		void checkAgain() {
			System.out.println("Hi...");
		}
	}

}

// 2nd useCase
/*class OuterA extends ParentA{  //OuterA can access ParentA
	private int x = 10;
	
	//inner class can access members of OuterA ++ ParentA ++ ParentB
	class InnerB extends ParentB{
		void check() {
			System.out.println(x);
		}
	}
}
*/
public class PreReq2 {

	public static void main(String[] args) {
		OuterA oA = new OuterA();
		
		OuterA.InnerB iB = oA.new InnerB(); // Non static inner class obj creation using Outer class objRef
		iB.check();
		
		OuterA.InnerC iC = new OuterA.InnerC(); // static inner class obj creation
		iC.checkAgain();
	}

}
