package com.abm.demoLambda1;

interface ISensor {
	void on();

	void off();

}

class TVSensor implements ISensor {

	@Override
	public void on() {
		System.out.println("**ON**");
	}

	@Override
	public void off() {
		System.out.println("**OFF**");
	}

}

public class PreReq1 {

	public static void main(String[] args) {
		// ISensor sen = new ISensor(); ==> Interface can not be instantiated directly
		ISensor sen = new TVSensor(); // Interface instantiated using implementation class == Generalization
		sen.on();
		sen.off();

	}

}
