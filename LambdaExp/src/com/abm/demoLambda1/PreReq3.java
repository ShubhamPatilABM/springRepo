// Anonymous Inner classes

package com.abm.demoLambda1;

//outer class  
class Hello {

	void saySomething() {
		System.out.println("Hello SP...");
	}
}

class Hi extends Hello {

	@Override
	void saySomething() {
		System.out.println("Good Afternoon...");
	}
}

public class PreReq3 {

	public static void main(String[] args) {
		Hello h1 = new Hello();
		h1.saySomething();

		Hello h2 = new Hi(); // Generalization ==> obj creation using Parent ret_type
		h2.saySomething();

		// Anonymous Inner class obj creation
		Hello h3 = new Hello() {
			@Override
			void saySomething() {
				System.out.println("Hi I'm  Anonymous Inner class...");
			}
		};
		h3.saySomething();
		
	
	}
}
