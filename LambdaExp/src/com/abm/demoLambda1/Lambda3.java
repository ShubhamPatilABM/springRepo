// Lambda 

package com.abm.demoLambda1;

@FunctionalInterface // optional to use
interface IMath1 {

	int calc(int x, int y);
}

public class Lambda3 {

	public static void main(String[] args) {

		IMath1 m2 = (int x, int y) -> x * y;
		System.out.println("Multiplication : " + m2.calc(20, 50));

	}
}
