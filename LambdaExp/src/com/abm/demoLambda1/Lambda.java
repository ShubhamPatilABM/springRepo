// Anonymous Inner classes

package com.abm.demoLambda1;

//outer class  
interface Printer {

	void print(String document);
}

class InkJetPrinter implements Printer {

	@Override
	public void print(String document) {
		System.out.println("InkJetPrinter Printing :" + document);
	}
}

public class Lambda {

	public static void main(String[] args) {
		Printer p1 = new InkJetPrinter();
		p1.print("abc.text");

		// Inner class implementing Printer
		class InkJetPrinter implements Printer {
			@Override
			public void print(String document) {
				System.out.println("InkJetPrinter Printing :" + document);
			}
		}
		Printer p2 = new InkJetPrinter();
		p2.print("abc.text");

		// Anonymous Inner class implementing Printer
		Printer p3 = new Printer() {
			@Override
			public void print(String document) {
				System.out.println("From Anonymous Inner Printing :" + document);
			}
		};
		p3.print("lmn.txt");

		// Lambda
		Printer p4 = (document) -> {
			System.out.println("Hey I'm Lambda Printing :-> " + document);
		};
		p4.print("lmn.txt");

		// Lambda oneLiner
		Printer p5 = (document) -> System.out.println("Hey I'm Lambda Printing :-> " + document); 
		p5.print("lmn.txt");
	}
}
