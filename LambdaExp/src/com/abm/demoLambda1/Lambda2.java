// Lambda 

package com.abm.demoLambda1;

interface IMath {

	int calc(int x, int y);
}

class MathImpl implements IMath {

	@Override
	public int calc(int x, int y) {
		System.out.println("Multiplication : " + (x * y));
		return x + y;
	}

}

public class Lambda2 {

	public static void main(String[] args) {

		IMath m1 = new IMath() {
			@Override
			public int calc(int x, int y) {	return x * y; }
		};
		System.out.println("Multiplication : " + m1.calc(20, 50));

		IMath m2 = (int x, int y) ->  x * y;
		System.out.println("Multiplication : "+m2.calc(20, 50));

	}
}
