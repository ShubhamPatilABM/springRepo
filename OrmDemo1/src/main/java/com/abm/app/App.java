package com.abm.app;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import om.abm.dao.CustomerDao;
import om.abm.entity.Customer;

public class App {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		String name = null;
		String email = null;
		
		System.out.println("*** Select Operation ***");
		int choice = 0;
		System.out.println("1. Insert Record" + "\n" + 
							"2. Fetch details by ID"+"\n"+
							"3. Fetch all records"+"\n"+
							"4. Fetch details by EmailId"+"\n"+
							"5. Fetch details by Month");
		choice = Integer.parseInt(s.nextLine());

		Customer customer = new Customer();
		CustomerDao dao = new CustomerDao();
		List<Customer> list = null;

		switch (choice) {
		case 1:
			System.out.println("Enter Name");
			name = s.nextLine();
			customer.setName(name);
			System.out.println("Enter Email");
			email = s.nextLine();
			customer.setEmail(email);
			
			System.out.println("Enter Year of birth");
			int yy = Integer.parseInt(s.nextLine());
			System.out.println("Enter Month of birth");
			int mm = Integer.parseInt(s.nextLine());
			System.out.println("Enter date of birth");
			int dd = Integer.parseInt(s.nextLine());
			customer.setDateOfBirth(LocalDate.of(yy, mm, dd));
			
			//store data
			dao.store(customer);
			System.out.println("Record Inserted successfuly...");
			break;

		case 2:
			System.out.println("Enter Customer ID : ");
			int id = Integer.parseInt(s.nextLine());
			customer = dao.fetch(id);
			System.out.println(customer.getName());
			System.out.println(customer.getEmail());
			System.out.println(customer.getDateOfBirth());
			break;
			
		case 3:
			list = dao.fetchAll();
			//list.forEach(det-> System.out.println(det));
			list.forEach(System.out::println);
			break;

		case 4:
			System.out.println("Enter Email Id :");
			email = s.nextLine();
			customer = dao.fetch(email);
			System.out.println(customer.getId());
			System.out.println(customer.getName());
			System.out.println(customer.getDateOfBirth());
			break;
		
		case 5:
			System.out.println("Enter Month of Birth :");
			int mon = s.nextInt();
			list = dao.fetchByMonth(mon);
			list.forEach(det-> System.out.println(det));
			break;
			
		default:
			System.out.println("Invalid selection");
			break;
		}

	}

}
