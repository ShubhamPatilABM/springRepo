package com.abm.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.abm.comp.interfaces.IAtm;
import com.abm.comp.interfaces.IBank;

@Component("hdfcAtm")
public class HDFCAtmImpl implements IAtm {

	@Autowired
	IBank bank;

	public void withdraw(int accno, double amount) {
		System.out.println("User at HDFC ATM want to withdraw : " + amount + " from a/c : " + accno);
		bank.withdraw(1122, accno, amount);
	}

}
