package com.abm.component;

import org.springframework.stereotype.Component;

import com.abm.comp.interfaces.IBank;

@Component
public class ICICIBankImpl implements IBank {

	public void withdraw(int atmId, int accno, double amount) {
		System.out.println(
				"User of ICICI Bank want to withdraw : " + amount + " from a/c : " + accno + " with ATMpin : " + atmId);
	}

}
