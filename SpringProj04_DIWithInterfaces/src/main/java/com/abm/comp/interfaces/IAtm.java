package com.abm.comp.interfaces;

public interface IAtm {
	
	public void withdraw(int accno, double amount);

}
