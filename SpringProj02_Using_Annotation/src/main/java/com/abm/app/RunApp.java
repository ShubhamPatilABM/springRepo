package com.abm.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abm.component.Calculator;

public class RunApp {
	
	public static void main(String[] args) {
		// loading the IOC container
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-webmvc.xml");
		
		// Accessing a bean
		Calculator c = (Calculator) ctx.getBean("calc");
		c.add(20, 50);
		
		
	}

}
