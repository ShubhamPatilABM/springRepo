package com.abm.component;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("calc") // alternate to ==>>>>   <bean id="calc" class="...Calculator"/>
//@Scope("singlTone")  >>>>>>  default scope,  only single object for a class
//@Scope("prototype")  >>>>>>  generates any no of objects as per call

public class Calculator {
	
	public void add(int x, int y) {
		System.out.println(x+y);
	}
	
	public void sub(int x, int y) {
		System.out.println(x-y);
	}

}
