package com.abm;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.abm.entity.Customer;
import com.abm.repository.CustomerRepository;

@SpringBootTest
class SpringBootAppApplicationTests {

	@Autowired
	private CustomerRepository custRepo;

	@Test
	void addCustomer() {

		Customer cust = new Customer();
		cust.setName("Chetan Aghawane");
		cust.setEmail("aghawane.chetan@gmail.com");
		cust.setPassword("Chetan07");
		cust.setMobileNumber("9082388437");

		custRepo.save(cust);
	}

}