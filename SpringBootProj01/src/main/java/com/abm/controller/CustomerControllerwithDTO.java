package com.abm.controller;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abm.dto.CustomerDetailsDto;
import com.abm.dto.RegistrationStatus;
import com.abm.entity.Customer;
import com.abm.exceptions.CustomerServiceException;
import com.abm.service.CustomerService;

@RestController
public class CustomerControllerwithDTO {
	
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping("/register_with_dto")
	public RegistrationStatus registerCustomer(@RequestBody CustomerDetailsDto customerDetailsDto) {
		RegistrationStatus status = new RegistrationStatus();
		try {
			Customer customer = new Customer();
			BeanUtils.copyProperties(customerDetailsDto, customer);
			
			customer.setPassword(Base64.getEncoder().encodeToString(customerDetailsDto.getPassword1().getBytes())); 
			
			customerService.register(customer);
			status.setStatus(true);
			status.setRegisteredCustomerId(customer.getId());
			status.setStatusMessage("Customer registered successfully...");
			return status;
			//return "Customer registered successfully...";
		}catch(CustomerServiceException exception){
			status.setStatus(false);
			status.setStatusMessage(exception.getMessage());
			//return exception.getMessage();
			return status;
		}	
	}
}
