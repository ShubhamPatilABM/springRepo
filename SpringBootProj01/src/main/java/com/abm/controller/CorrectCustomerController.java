package com.abm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abm.dto.RegistrationStatus;
import com.abm.entity.Customer;
import com.abm.exceptions.CustomerServiceException;
import com.abm.service.CustomerService;

@RestController
public class CorrectCustomerController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping("/register_new")
	public RegistrationStatus registerCustomer(@RequestBody Customer customer) {
		RegistrationStatus status = new RegistrationStatus();
		try {
			customerService.register(customer);
			status.setStatus(true);
			status.setRegisteredCustomerId(customer.getId());
			status.setStatusMessage("Customer registered successfully...");
			return status;
			// return "Customer registered successfully...";
		} catch (CustomerServiceException exception) {
			status.setStatus(false);
			status.setStatusMessage(exception.getMessage());
			// return exception.getMessage();
			return status;
		}
	}
}
