package com.abm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abm.dto.RegistrationStatus;
import com.abm.entity.Customer;
import com.abm.exceptions.CustomerServiceException;
import com.abm.service.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping("/register")
	public String registerCustomer(@RequestBody Customer customer) {
		try {
			customerService.register(customer);
			return "Customer registered successfully...";
		} catch (CustomerServiceException exception) {
			return exception.getMessage();
		}
	}

}