<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="openAccount">
		<h1>Open New Account</h1>
		<div>
			Enter Name : <input name="name"> <br>
		</div><br>
		<div>
			Account Type : 
			<select name="accType">
				<option value="Saving">Saving</option>
				<option value="Current">Current</option>
			</select>
		</div><br>
		<div>
			Initial Balance : <input name="balance"> <br>
		</div><br>
		<div>
			<button type="submit">Open Account</button>
			<button type="submit"><a href="index.jsp">Home</a></button>
		</div>
	</form>
</body>
</html>