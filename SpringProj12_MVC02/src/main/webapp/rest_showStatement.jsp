<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
<script type="text/javascript">
	$(function(){
		$("form").submit(function(event){
			event.preventDefault();
			//let pnrNo = $('#pnrNo').val();
			let url = 'http://localhost:8081/SpringProj12_MVC02/rest_getDetails?accNo='+  $('#accNo').val();
			$.ajax({
				url : url,
				method : 'GET',
				success : function(data){
					alert(JSON.stringify(data));
					let resultDiv = $("#result");
					//resultDiv.append("<p />").text("Train No "+data.trainNo);
					//resultDiv.append("<p />").text("Trael Date "+data.travelDate);
					//$.each(data.passengers, function(passenger, index){
					for(let t of data.transactions){
						let p = $("<p />")
										.html("Tx No : "+t.txNo+"<br>" 
										+"Tx Amt : "+t.txAmount+"<br>"
										+"Tx Date : "+t.txDate+"<br>"
										+"Tx Type : "+t.txType+"<br>"
										+"<hr />");
						resultDiv.append(p);
					} 
				}
			})
		})
	})
</script>

<style type="text/css">
table, th, td {
	border: 1px solid black;
}
</style>

</head>
<body>
	<form action="rest_getDetails">
		<h2>Get Account Statement</h2>
		<div>
			Enter Account No : <input id="accNo">
		</div>
		<div>
			<button type="submit">Get Statement</button>
			<button type="submit"><a href="index.jsp">Home</a></button>
		</div>
	</form>
	<div id="result"></div>
<%-- 	<div>
		<c:if test="${ account != null}">
			Search Result : 
			Acc. No : ${ account.accNo} <br>
			Name : ${ account.name} <br>
			Acc. Type : ${ account.accType} <br>
			Available Balance : ${ account.balance} <br>
			<br>
			
			Account Statement :
			<table>
				<tr>
					<th>Tx No</th>
					<th>Tx Amt</th>
					<th>Tx Date</th>
					<th>Tx Type</th>
				</tr>
				<c:forEach items="${account.transactions}" var="t">
					<tr>
						<td>${t.txNo}</td>
						<td>${t.txAmount}</td>
						<td>${t.txDate}</td>
						<td>${t.txType}</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
	</div> --%>
</body>
</html>