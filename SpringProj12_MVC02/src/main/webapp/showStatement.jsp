<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
table, th, td {
	border: 1px solid black;
}
</style>

</head>
<body>
	<form action="getDetails">
		<h2>Get Account Statement</h2>
		<div>
			Enter Account No : <input type="text" name="accNo">
		</div> <br>
		<div>
			<button type="submit">Get Statement</button>
			<button type="submit"><a href="index.jsp">Home</a></button>
		</div>
	</form>
	<div>
		<c:if test="${ account != null}">
			Search Result : 
			Acc. No : ${ account.accNo} <br>
			Name : ${ account.name} <br>
			Acc. Type : ${ account.accType} <br>
			Available Balance : ${ account.balance} <br>
			<br>
			
			Account Statement :
			<table>
				<tr>
					<th>Tx No</th>
					<th>Tx Amt</th>
					<th>Tx Date</th>
					<th>Tx Type</th>
				</tr>
				<c:forEach items="${account.transactions}" var="t">
					<tr>
						<td>${t.txNo}</td>
						<td>${t.txAmount}</td>
						<td>${t.txDate}</td>
						<td>${t.txType}</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
	</div>
</body>
</html>