package com.abm.exceptions;

public class BankingException extends Exception {
	
	public BankingException(String string) {
		super(string);
	}
}
