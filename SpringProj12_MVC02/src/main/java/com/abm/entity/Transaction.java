package com.abm.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tbl_acc_transaction")
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int txNo;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDateTime txDate;

	private String txType;

	private double txAmount;

	@JsonIgnore // to stop json infinite loop
	@ManyToOne
	@JoinColumn(name = "accNo")
	private Account account;

	public int getTxNo() {
		return txNo;
	}

	public void setTxNo(int txNo) {
		this.txNo = txNo;
	}

	public LocalDateTime getTxDate() {
		return txDate;
	}

	public void setTxDate(LocalDateTime txDate) {
		this.txDate = txDate;
	}

	public String getTxType() {
		return txType;
	}

	public void setTxType(String txType) {
		this.txType = txType;
	}

	public double getTxAmount() {
		return txAmount;
	}

	public void setTxAmount(double txAmount) {
		this.txAmount = txAmount;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

}
