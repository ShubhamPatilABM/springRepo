package com.abm.service;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.abm.dao.BankingDao;
import com.abm.entity.Account;
import com.abm.entity.Transaction;
import com.abm.exceptions.BankingException;

@Component
//Or
//@Service ==>>> Recommended
@Transactional // Business logic should be in transaction
public class BankingService {

	@Autowired
	private BankingDao bankingDao;

	public int openAcount(Account account) throws BankingException {
		if (account.getBalance() >= 5000)
			return bankingDao.addAccount(account);
		else
			throw new BankingException("Cannot open Account !!!");
	}

	public void transfer(int fromAcNo, int toAcNo, double txAmount) {
		Account fromAccount = bankingDao.fetchAcc(fromAcNo);
		Account toAccount = bankingDao.fetchAcc(toAcNo);

		fromAccount.setBalance(fromAccount.getBalance() - txAmount);
		toAccount.setBalance(toAccount.getBalance() + txAmount);

		bankingDao.update(fromAccount);
		bankingDao.update(toAccount);

		Transaction t1 = new Transaction();
		t1.setAccount(fromAccount);
		t1.setTxType("Debit");
		t1.setTxDate(LocalDateTime.now());
		t1.setTxAmount(txAmount);

		Transaction t2 = new Transaction();
		t2.setAccount(toAccount);
		t2.setTxType("Credit");
		t2.setTxDate(LocalDateTime.now());
		t2.setTxAmount(txAmount);

		bankingDao.add(t1);
		bankingDao.add(t2);

	}

	public Account fetchAccTrxDetails(int accNo) {
		return bankingDao.fetchAccWithTrxDet(accNo);
	}

}
