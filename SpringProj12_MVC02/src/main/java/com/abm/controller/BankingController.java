package com.abm.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abm.entity.Account;
import com.abm.entity.Transaction;
import com.abm.exceptions.BankingException;
import com.abm.service.BankingService;

@Controller
public class BankingController {

	@Autowired
	private BankingService bankingService;

	/*  Spring can automatically copy the data coming from the client 
		in an Object of some class. so no need to use @RequestParam */
	@RequestMapping("/openAccount")
	public String openAc(Account account, Map<String, Object> map) {
		try {
			int accNo = bankingService.openAcount(account);
			map.put("generatedAccNo", accNo);
			return "confirmation.jsp";
		} catch (BankingException e) {
			return "openAccount.jsp";
		}
	}
	
	@RequestMapping("/transferMoney")
	public String transfer(@RequestParam("fromAcNo") int fromAcNo,
						   @RequestParam("toAcNo") int toAcNo,
						   @RequestParam("txAmount") double txAmount) {
		bankingService.transfer(fromAcNo, toAcNo, txAmount);
		return "transactionSuccess.jsp";
	}
	
	@RequestMapping("/getDetails")
	public String getDetails(@RequestParam("accNo") int accNo, Map<String, Object> map) {
		Account account = bankingService.fetchAccTrxDetails(accNo);
		map.put("account", account);
		
		return "showStatement.jsp";
	}

}
