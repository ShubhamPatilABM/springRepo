package com.abm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abm.entity.Account;
import com.abm.service.BankingService;

@RestController
public class GetBankDetailsRestController {
	@Autowired
	private BankingService bankingService;

	@RequestMapping(path="/rest_getDetails", produces = "application/json")
	public Account getDetails(@RequestParam("accNo") int accNo) {
		Account account = bankingService.fetchAccTrxDetails(accNo);
		return account;
	}

}
