package com.abm.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import com.abm.entity.Account;
import com.abm.entity.Transaction;

@Component
//OR
//@Repository ==>>Recommended
public class BankingDao {

	@PersistenceContext
	private EntityManager entityManager;

	public int addAccount(Account account) {
		// entityManager.persist(account);
		// OR
		// merge() ==> is used in both insert and update operations both
		Account updateAccount = (Account) entityManager.merge(account);
		return updateAccount.getAccNo();
	}

	public Account fetchAcc(int accNo) {
		return entityManager.find(Account.class, accNo);
		// OR
		/*return (Account) 
				entityManager
				.createQuery("SELECT a.balance FROM Account a where a.accNo =: accNo")
				.setParameter("accNo", accNo)
				.getSingleResult();
		*/
	}

	public void update(Account account) {
		entityManager.merge(account);
	}

	public void add(Transaction t) {
		entityManager.persist(t);
	}

	public Account fetchAccWithTrxDet(int accNo) {
		// return entityManager.find(Account.class, accNo);
		// OR
		return (Account) entityManager
				.createQuery("SELECT distinct a FROM Account a join fetch a.transactions t where a.accNo =: accNo")
				.setParameter("accNo", accNo)
				.getSingleResult();
		
		//OR
		/*return entityManager
				.createQuery("SELECT distinct a FROM Account a join a.transactions t where a.accNo =: accNo", Account.class)
				.setParameter("accNo", accNo)
				.getSingleResult();*/
	}

}
