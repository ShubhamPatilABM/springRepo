package com.abm.demoInherit;

import java.time.LocalDate;

public class Payroll {
	
	private void doSalary(Employee emp) {
		System.out.println("Salary Rs. "+emp.getSalary()+" issued to "+emp.getName()+" sucessfully");
	}
	
	public static void main(String[] args) {
		//public Manager(int empno, String name, double salary, LocalDate dateOfJoining, double incentives)
		Manager mgr = new Manager(101, "Alen", 63000.00, LocalDate.of(2022,3,22), 6000.00);
		//public Developer(int empno, String name, double salary, LocalDate dateOfJoining, int overtime)
		Developer dev = new Developer(111, "Sam", 32000, LocalDate.of(2022, 2,11),2);
		
		Payroll p = new Payroll();
		p.doSalary(mgr);
		p.doSalary(dev);
		

	}
}
