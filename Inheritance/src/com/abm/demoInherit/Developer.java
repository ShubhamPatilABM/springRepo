package com.abm.demoInherit;

import java.time.LocalDate;

public class Developer extends Employee {

	private double overtime;

	// 0-param or default Constructor
	public Developer() {
	}

	// invoked superClass fields
	public Developer(int empno, String name, double salary, LocalDate dateOfJoining, int overtime) {
		// Parent class constructor call ==> super();
		super(empno, name, salary, dateOfJoining);
		this.overtime = overtime;
	}

	@Override //Overriding getSalary() of Employee class 
	public double getSalary() {
		return super.getSalary()+getOvertime()*150;
	}

	public double getOvertime() {
		return overtime;
	}

	public void setOvertime(double overtime) {
		this.overtime = overtime;
	}

}
