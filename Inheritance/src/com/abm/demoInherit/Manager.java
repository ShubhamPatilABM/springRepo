package com.abm.demoInherit;

import java.time.LocalDate;

public class Manager extends Employee {

	private double incentives;

	// 0-param or default Constructor
	public Manager() {
	}

	// invoked superClass fields
	public Manager(int empno, String name, double salary, LocalDate dateOfJoining, double incentives) {
		// Parent class constructor call ==> super();
		super(empno, name, salary, dateOfJoining);
		this.setIncentives(incentives);
	}
	
	@Override //Overriding getSalary() of Employee class
	public double getSalary() {
		return super.getSalary()+getIncentives();
	}

	public double getIncentives() {
		return incentives;
	}

	public void setIncentives(double incentives) {
		this.incentives = incentives;
	}

}
