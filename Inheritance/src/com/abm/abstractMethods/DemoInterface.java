package com.abm.abstractMethods;

public interface DemoInterface {
	
	final static String panNo = "DYRPP2455N";
	
	//not allowed to have Constructor
	//public  DemoInterface() {}
	
	public void mul(int x, int y);

	public void div(int x, int y);

}
