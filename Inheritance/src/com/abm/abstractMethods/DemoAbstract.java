package com.abm.abstractMethods;

public abstract class DemoAbstract {
	
	//allows to have Constructor
	public DemoAbstract() {
		System.out.println("hi....");
	}
	
	public abstract void add(int x, int y);

	public abstract void sub(int x, int y);

}
