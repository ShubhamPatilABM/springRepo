package com.abm.abstractMethods;

public class MainImpl {

	public static void main(String[] args) {
		Calculator c = new Calculator();
		c.add(20, 10);
		c.sub(35, 7);
		c.mul(28, 44);
		c.div(23, 5);
		// 
		//MainImpl.panNo
		//System.out.println(panNo);  ==> we could access this Interface variable in implementation class Not in Main class
		System.out.println(c.panNo);
		System.out.println(Calculator.panNo); //accessing Interface variable using implementation class name
		System.out.println(DemoInterface.panNo); //accessing Interface variable using Interface name
	}

}
