package com.abm.abstractMethods;

public class Calculator extends DemoAbstract implements DemoInterface {
	
	

	public Calculator() {
		//super(a);
		
	}

	@Override
	public void add(int x, int y) {
		System.out.println("Addition is : " + (x + y));
	}

	@Override
	public void sub(int x, int y) {
		System.out.println("Substraction is : " + (x - y));

	}

	@Override
	public void mul(int x, int y) {
		System.out.println("Multiplication is : " + (x * y));

	}

	@Override
	public void div(int x, int y) {
		System.out.println("Division is : " + (x / y));

	}

}
