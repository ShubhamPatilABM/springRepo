package com.abm.useOfKeywords;

public class EmpMain {

	public static void main(String[] args) {

		EmpNoFinal e = new EmpNoFinal(101, "Chetan", 45000.0);
		EmpNoFinal e1 = new EmpNoFinal(102, "Vikalp", 45000.0);

		System.out.println("Organization : " + EmpNoFinal.COMPANY_NAME + "\nEmployee Id : " + e.getEmpno()
				+ "\nEmployee Name : " + e.getName() + "\nSalary : " + e.getSalary());
		System.out.println("************************************");
		System.out.println("Organization : " + EmpNoFinal.COMPANY_NAME + "\nEmployee Id : " + e1.getEmpno()
				+ "\nEmployee Name : " + e1.getName() + "\nSalary : " + e1.getSalary());

	}

}
