package com.abm.useOfKeywords;

public class EmpNoFinal {

	// private final int empno=101; not a good practice, will result in wrong OP
	private final int empno;
	private String name;
	private double salary;
	
	//for static var only one copy is created
	public static final String COMPANY_NAME="ABM Knowledgeware Ltd";
	
	// Initialize final var using constructor
	public EmpNoFinal(int empno) {
		this.empno = empno;
	}

	public EmpNoFinal(int empno, String name, double salary) {
		this.empno = empno;
		this.name = name;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	//For final var only getter() is available, i.e. final var must not be modified once initialized
	public int getEmpno() {
		return empno;
	}

}
