package com.abm.demoException;

import java.util.Scanner;

class BankAcc {
	int acno;
	double balance;

	BankAcc(int acno, int balance) {
		this.acno = acno;
		this.balance = balance;
	}

	double withdraw(double amount) throws AccountException {
		if (amount > balance) {
			AccountException e = new AccountException("Insufficient Balance !...");
			throw e;
		} else {
			balance -= amount; // balance = balance - amount;
			return balance;
		}
	}
}

class AccountException extends Exception {
	public AccountException(String msg) {
		super(msg);
	}

}

public class UserDefException {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		BankAcc ba = new BankAcc(111, 5000);
		try {
			System.out.println("Please enter amount");
			double amount = s.nextDouble();
			s.close();
			double balanceb = ba.withdraw(amount);
			System.out.println("Transaction completed....");
			System.out.println("Balance amount : " + balanceb);
		} catch (AccountException e) {
			e.printStackTrace();
			System.out.println(e.getMessage() + "\n" + "available balace is Rs : " + ba.balance);
		}
	}
}
