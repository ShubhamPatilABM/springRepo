package com.abm.demoException;

import java.util.Scanner;

class BankAccount {

	int acno;
	double balance;

	BankAccount(int acno, int balance) {
		this.acno = acno;
		this.balance = balance;
	}

	double withdraw(double amount) throws Exception {
		if (amount > balance) {
			//Exception e = new Exception("Insufficient Balance !...");
			RuntimeException e = new RuntimeException("Insufficient Balance !...");
			throw e;
		} else {
			balance -= amount; // balance = balance - amount;
			return balance;
		}
	}
}

public class Example2 {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		BankAccount ba = new BankAccount(111, 5000);
		try {
			System.out.println("Please enter amount");
			double amount = s.nextDouble();
			s.close();
			double balance = ba.withdraw(amount);
			System.out.println("Transaction completed....");
			System.out.println("Balance amount : " + balance);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage()+"\n"+"available balace is Rs : "+ba.balance);
		}

	}

}
