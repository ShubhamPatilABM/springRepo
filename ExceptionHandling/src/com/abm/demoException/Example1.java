package com.abm.demoException;

public class Example1 {

	public static void main(String[] args) {

		/*
		 * int[] arr = { 10, 20, 30 }; System.out.println(arr[6]);
		 */
		try {
			int[] arr = { 10, 20, 30 };
			System.out.println(arr[6]);
//		} catch (ArrayIndexOutOfBoundsException ae) {
		} catch (Exception ae) {
			System.out.println(ae.getMessage());
			System.out.println(ae.getClass());
			ae.printStackTrace();
			System.out.println(ae.getCause());
			System.out.println(ae.hashCode());
			System.out.println("Please check no. of glasses");
		}

		// java.lang.NullPointerException
		/*
		 * String s = null; s.toUpperCase();
		 */

		try {
			int x = Integer.parseInt("100kkk");
			System.out.println(x);
		} catch (NumberFormatException ne) {
			System.out.println("Incorrect Number");
		}

	}

}
