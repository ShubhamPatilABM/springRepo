package om.abm.entity.bi_association;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_passport")
public class Passport {
	
	@Id //PK
	@GeneratedValue  
	@Column(name = "pass_id")
	private long passId;

	@Column(name = "issue_date")
	private LocalDate issueDate;
	
	@Column(name = "expiry_date")
	private LocalDate expDate;
	
	@Column(name = "issued_by")
	private String issuedBy;
	
	// Bidirectional Association
	@OneToOne(cascade = CascadeType.ALL)   // one to one Association or Has-A-Relation
	@JoinColumn(name = "p_id") 
	private Person person;

	public long getPassId() {
		return passId;
	}

	public void setPassId(long passId) {
		this.passId = passId;
	}

	public LocalDate getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(LocalDate issueDate) {
		this.issueDate = issueDate;
	}

	public LocalDate getExpDate() {
		return expDate;
	}

	public void setExpDate(LocalDate expDate) {
		this.expDate = expDate;
	}

	public String getIssuedBy() {
		return issuedBy;
	}

	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
