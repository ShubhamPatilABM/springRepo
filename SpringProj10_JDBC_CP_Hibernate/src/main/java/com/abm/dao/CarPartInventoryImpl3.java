package com.abm.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Component;

import com.abm.dao.interfaces.ICarPartInventory;
import com.abm.entity.CarPart;

@Component("carPartsInv3")
public class CarPartInventoryImpl3 implements ICarPartInventory {

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public void addNewPArt(CarPart carPart) {
		em.persist(carPart);
		System.out.println("Record successfully inserted....");
	}

	public List<CarPart> getAvailableParts() {
		/*
			String query = "SELECT cp FROM CarPart cp";
			Query q = em.createQuery(query);
			List<CarPart> list = q.getResultList();
			return list;
		*/
		
			//OR
		//return em.createQuery("Select cp from CarPart cp").getResultList();
		
			//OR
		return em
				.createQuery("Select cp from CarPart cp")
				.getResultList();

	}

}
