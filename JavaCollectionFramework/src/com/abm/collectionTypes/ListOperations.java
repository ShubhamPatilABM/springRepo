package com.abm.collectionTypes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;

public class ListOperations {

	public static void main(String[] args) {
		List<String> st = new ArrayList<>(); // Generalization
		st.add("Vikalp");
		st.add("Pritam");
		st.add("Chetan");
		st.add("Dharmesh");

		System.out.println("======= Using Traditional for loop ========");
		for (int i = 0; i < st.size(); i++) {
			String slist = st.get(i);
			System.out.println(slist);
		}

		System.out.println("======= Using Enhanced for loop ========");
		for (String obj : st) {
			System.out.println(obj);
		}

		System.out.println("======= Using Lambda Expression ========");
		st.forEach(olambda -> System.out.println(olambda));

		System.out.println("======= Using Traditional Iterator API ========");
		Iterator<String> itr = st.iterator();
		while (itr.hasNext()) {
			Object obj = itr.next();
			System.out.println(obj);
		}

	}

}
