package com.abm.collectionTypes;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public class DemoList {

	public static void main(String[] args) {

		System.out.println("======= ArrayList ==========");

		// Creating a List of type String using ArrayList
		ArrayList<String> list1 = new ArrayList<>();

		list1.add("Chetan");
		list1.add("Arif");
		list1.add("Pritam");
		list1.add("Pritam");
		list1.add("Vikalp");
		// Enhanced for loop
		for (String str : list1) {
			System.out.println(str);
		}
		// 5th index element
		System.out.println("5th Position Element : " + list1.get(4));
		// sorting
		Collections.sort(list1);
		System.out.println("after sorting : " + list1);

		System.out.println("======= LinkedList ==========");
		LinkedList<String> lst = new LinkedList<>();
		lst.add("Chetan");
		lst.add("Arif");
		lst.add("Pritam");
		lst.add("Pritam");
		lst.add("Vikalp");
		lst.add(1, "Shubham");
		for (String s : lst) {
			System.out.println(s);
		}
		//sorting ~~ Ascending order
		Collections.sort(lst);
		System.out.println("After Sorting : "+lst);
		//descending order
		Collections.reverse(lst);
		System.out.println("In descending order : "+lst);
		
		//copying Elements of one collection to another  == > list1 into lst == > lst = lst + list1
		lst.addAll(list1);
		System.out.println(lst);
		
	}

}
