package com.abm.collectionTypes;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import com.abm.collection2.Person;

public class DemoSet {

	public static void main(String[] args) {

		System.out.println("======= HashSet ==========");

		// Creating a List of type String using ArrayList
		Set<String> set = new HashSet<>();
		set.add("Chetan");
		set.add("Arif");
		set.add("Vikalp");
		// Enhanced for loop
		for (String names : set) {
			System.out.print(names + "\t");
		}

		System.out.println("\n======= LinkedHashSet ==========");
		Set<String> lset = new LinkedHashSet<>();
		lset.add("Chetan");
		lset.add("Arif");
		lset.add("Pritam");
		lset.add("Pritam");
		for (String names : lset) {
			System.out.print(names + "\t");
		}

		System.out.println("\n======= TreeSet ==========");
		Set<String> tset = new TreeSet<>();
		tset.add("Chetan");
		tset.add("Pritam");
		tset.add("Pritam");
		tset.add("Vikalp");
		for (String names : tset) {
			System.out.print(names + "\t");
		}

		System.out.println("\n=================Set====================");
		Set<Person> pset = new HashSet<>(); //Generalization
		pset.add(new Person("Chetan", 26, 9878999888l));
		pset.add(new Person("Pritam", 25, 9975449888l));
		pset.add(new Person("Chetan", 26, 9878999888l)); //duplicate elements
		pset.add(new Person("Dharmesh", 24, 7879999888l));
		System.out.println("======= Using Lambda Expression ========");
		pset.forEach(a -> System.out.println(a.getName()+" "+a.getAge()+" "+a.getMoNo()));
		
	}

}
