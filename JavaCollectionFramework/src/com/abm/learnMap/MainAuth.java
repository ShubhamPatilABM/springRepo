package com.abm.learnMap;

import java.util.HashMap;
import java.util.Map;

class Users {

	private String username;
	private String password;

	public Users(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + "]";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

class MainAuthUsers {

	private HashMap<String, String> users = new HashMap<>();

	MainAuthUsers() {

		users.put("Chetan", "1111");
		users.put("Pritam", "2222");
		users.put("Dharmesh", "3333");
	}

	boolean isValidUser(String username, String password) {
		for (Map.Entry<String, String> entry : users.entrySet())
			if (entry.getKey().equals(username) && entry.getValue().equals(password))
				return true;
		return false;
		
		/*String pw = users.get(username);
		if(pw != null && pw.equals(password)) 
			return true;
		return false;*/
	}

}

public class MainAuth {

	public static void main(String[] args) {

		MainAuthUsers um = new MainAuthUsers();
		boolean isValid = um.isValidUser("Chetan", "1111");
		if (isValid)
			System.out.println("Valid user .... ");
		else
			System.out.println("Invalid user .... ");
	}

}
