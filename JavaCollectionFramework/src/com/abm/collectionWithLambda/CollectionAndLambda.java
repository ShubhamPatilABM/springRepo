package com.abm.collectionWithLambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionAndLambda {

	public static void main(String[] args) {

		List<Emp> list = new ArrayList<>();
		list.add(new Emp(11, "Vikalp", 86699));
		list.add(new Emp(22, "Pritam", 99754));
		list.add(new Emp(11, "Vikal", 99699));
		list.add(new Emp(22, "Pritam", 99754));
		list.add(new Emp(33, "Chetan", 98789));
		list.add(new Emp(44, "Dharmesh", 78799));

		System.out.println("========= using Collections.sort by EmpNo ================");
		Collections.sort(list, (e1, e2) -> e1.empno - e2.empno);
		list.forEach(System.out::println);

		System.out.println("\n========= using list.sort by EmpNo ================");
		list.sort((e1, e2) -> e1.empno - e2.empno);
		list.forEach(System.out::println);

		System.out.println("\n========= using list.sort by EmpNo Descending ================");
		list.sort((e1, e2) -> e2.empno - e1.empno);
		list.forEach(System.out::println);

		System.out.println("\n========= using list.sort by EmpName with IgnoreCase ================");
		list.sort((e1, e2) -> e1.ename.compareToIgnoreCase(e2.ename));
		list.forEach(System.out::println);

		System.out.println("\n========= using list.sort by EmpName ================");
		list.sort((e1, e2) -> e1.ename.compareTo(e2.ename));
		list.forEach(System.out::println);

		System.out.println("\n========= using list.sort by Salary ================");
		list.sort((e1, e2) -> Double.compare(e1.salary, e2.salary));
		list.forEach(System.out::println);

		/*System.out.println("\n========= using list.sort by All Params ================");
		list.sort((e1, e2) -> {
			int order = e1.ename.compareTo(e2.ename);
			if (order == 0) {
				order = Double.compare(e1.salary, e2.salary);
			}
			return order;
		});
		list.forEach(System.out::println);*/

		System.out.println("\n========= using list.sort by All Params ================");
		list.sort((e1, e2) -> {
			int order = e1.empno - e2.empno;
			if (order == 0) {
				order = e1.ename.compareTo(e2.ename);
				if (order == 0) {
					order = Double.compare(e1.salary, e2.salary);
				}
			}
			return order;
		});
		list.forEach(System.out::println);

	}

}

class Emp {
	int empno;
	String ename;
	double salary;

	Emp(int empno, String ename, int salary) {
		this.empno = empno;
		this.ename = ename;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Emp [empno=" + empno + ", ename=" + ename + ", salary=" + salary + "]";
	}

}