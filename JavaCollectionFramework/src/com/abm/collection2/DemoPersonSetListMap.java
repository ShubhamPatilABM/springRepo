package com.abm.collection2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DemoPersonSetListMap {

	public static void main(String[] args) {

		System.out.println("================= List ====================");

		List<Person> pl = new ArrayList<>(); // Generalization
		Person p = new Person("Arif", 23, 9999999888l);
		pl.add(p);
		pl.add(new Person("Vikalp", 24, 8669999888l));
		pl.add(new Person("Pritam", 25, 9975449888l));
		pl.add(new Person("Chetan", 26, 9878999888l));
		pl.add(new Person("Dharmesh", 24, 7879999888l));
		for (Person pd : pl) {
			System.out.println(
					"Name : " + pd.getName() + "\t" + "Age : " + pd.getAge() + " " + "Contact No : " + pd.getMoNo());
		}

		System.out.println("================= Set ====================");
		Set<Person> set = new HashSet<>(); // Generalization
		set.add(new Person("Chetan", 26, 9878999888l));
		set.add(new Person("Pritam", 25, 9975449888l));
		set.add(new Person("Chetan", 26, 9878999888l));
		set.add(new Person("Dharmesh", 24, 7879999888l));
		for (Person pd : set) {
			System.out.println(
					"Name : " + pd.getName() + "\t" + "Age : " + pd.getAge() + " " + "Contact No : " + pd.getMoNo());
		}
		set.forEach(per -> System.out.println(per));
		// using method reference
		System.out.println("========= using method reference =============");
		set.forEach(System.out::println);

		System.out.println("================= Map ====================");
		Map<Long, Person> map = new HashMap<>(); // Generalization
		map.put(123411232323L, new Person("Chetan", 26, 9878999888l));
		map.put(343411232323L, new Person("Pritam", 25, 9975449888l));
		map.put(454511232323L, new Person("Chetan", 26, 9878999888l));
		map.put(986868932323L, new Person("Dharmesh", 24, 7879999888l));
		/*Person per = map.get(123411232323L);
		System.out.println(per);*/

		/*for (Map.Entry<Long, Person> entry : map.entrySet()) {
			Long aadhar = entry.getKey();
			Person prs = entry.getValue();
			System.out.println(aadhar + " : " + prs);
		}*/
		//map.forEach((k, v) -> i[0] += k + v);
		map.forEach((aadhar, per) ->  System.out.println(aadhar + " : " + per));
	}

}
