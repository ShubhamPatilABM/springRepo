package com.abm.collection2;

public class Person {
	private String name;
	private int age;
	private long moNo;

	public Person(String name, int age, long moNo) {
		super();
		this.name = name;
		this.age = age;
		this.moNo = moNo;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", moNo=" + moNo + "]";
	}

	@Override
	public int hashCode() {
		System.out.println("Person.hashCode()");
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + (int) (moNo ^ (moNo >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		System.out.println("Person.equals()");
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (age != other.age)
			return false;
		if (moNo != other.moNo)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public long getMoNo() {
		return moNo;
	}

	public void setMoNo(long moNo) {
		this.moNo = moNo;
	}

}
