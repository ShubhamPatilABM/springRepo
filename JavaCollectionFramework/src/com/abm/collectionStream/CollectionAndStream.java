package com.abm.collectionStream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionAndStream {

	public static void main(String[] args) {

		List<Emp> list = new ArrayList<>();
		list.add(new Emp(11, "Vikalp", 86699));
		list.add(new Emp(11, "Vikal", 70699));
		list.add(new Emp(22, "Pritam", 79754));
		list.add(new Emp(33, "Chetan", 98789));

		System.out.println("************* use Stream on Collection *********************");
		Stream<Emp> s = list.stream();
		// s.forEach(emp -> System.out.println(emp) );
		System.out.println("1st wat to use stream " + "\nList of Employes having salary >= 90000 ");
		s = s.filter(emp -> emp.salary >= 90000);
		s.forEach(emp -> System.out.println(emp));

		System.out.println("\n2nd wat to use stream " + "\n******* List of Employes names having 'a' ************");
		list.stream().filter(emp -> emp.ename.contains("a")).forEach(System.out::println);

		System.out.println("\n********** List of Employes having salary between 80000 ~ 90000 ************");
		List<Emp> list2 = list.stream().filter(emp -> emp.salary >= 80000 && emp.salary <= 90000)
				.collect(Collectors.toList());
		list2.forEach(emp -> System.out.println(emp));

		System.out.println("\n********** List of Employes having salary greater than 95000 ************");
		List<Emp> list3 = list.stream().filter(emp -> emp.salary >= 95000)
				// .sorted(Comparator.comparing(Emp::ename)) //Error
				.sorted(Comparator.comparing(Emp::getEname))
				// OR ==> .sorted((e1, e2) -> e1.ename.compareTo(e2.ename))
				.collect(Collectors.toList());
		list3.forEach(emp -> System.out.println(emp));
		
		System.out.println("\n********** only List of Employes names ************");
		List<String> names = list.stream()
				.map(emp -> emp.ename)  // map is used for transformation, to get particular data from list
				.collect(Collectors.toList());
		names.forEach(emp -> System.out.println(emp));
		
		System.out.println("\n********** Total Salary ************");
		Double totalSalary = list.stream()
								.mapToDouble(Emp::getSalary)
								.sum();
		System.out.println("Total Salary of All Employees : Rs "+totalSalary);

	}

}

class Emp {
	int empno;
	String ename;
	double salary;

	Emp(int empno, String ename, int salary) {
		this.empno = empno;
		this.ename = ename;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Emp [empno=" + empno + ", ename=" + ename + ", salary=" + salary + "]";
	}

	public int getEmpno() {
		return empno;
	}

	public void setEmpno(int empno) {
		this.empno = empno;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

}