package com.abm.learnCollection;

import java.util.ArrayList;
import java.util.List;

public class UserManager {

	boolean valid;

	List<User> users = new ArrayList<>();

	public UserManager() {

		users.add(new User("Chetan", "1111"));
		users.add(new User("Pritam", "2222"));
		users.add(new User("Dharmesh", "3333"));
	}

	boolean isValidUser(String username, String password) {
		for (User u : users)
			if (u.getUsername().equals(username) && u.getPassword().equals(password))
				return true;
		return false;
	}

}
