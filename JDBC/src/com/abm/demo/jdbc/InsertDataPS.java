package com.abm.demo.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class InsertDataPS {

	public static void main(String[] args) {

		Connection conn = null;
		//Statement stmt = null;
		PreparedStatement ps = null;
		Scanner s = new Scanner(System.in);
		try {
			// load driver
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			// create connection
			conn = DriverManager.getConnection("jdbc:derby://localhost:1527/trining", "derby", "derby");
			System.out.println("*** DB Connection is successful .... ***\n");

			/*stmt = conn.createStatement();
			stmt.executeUpdate("INSERT INTO tbl_product VALUES (105,'Diwali Lamp', 599.99 , 5)");
			System.out.println("Record successfully inserted....");
			*/

			String query = "INSERT INTO tbl_product VALUES (?,?,?,?)";
			ps = conn.prepareStatement(query);

			System.out.println("Enter no of Products to be registered : ");
			int pcount = Integer.parseInt(s.nextLine());
			for (int i = 1; i <= pcount; i++) {
				System.out.println("Enter Product Id: ");
				int id = Integer.parseInt(s.nextLine());
				System.out.println("Enter Product Name: ");
				String pName = s.nextLine();
				System.out.println("Enter Product Price: ");
				double price = Double.parseDouble(s.nextLine());
				System.out.println("Enter Product Qnty: ");
				int qty = Integer.parseInt(s.nextLine());
				s.close();

				// substitute positional params with values
				ps.setInt(1, id);
				ps.setString(2, pName);
				ps.setDouble(3, price);
				ps.setInt(4, qty);
				// execute query
				ps.executeUpdate();
			}
			System.out.println("Record successfully inserted....");

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();

		}
	}

}
