package com.abm.demo.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class RetrieveDataPS {

	public static void main(String[] args) {

		Connection conn = null;
		PreparedStatement ps = null;
		Scanner s = new Scanner(System.in);
		try {
			// load driver
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			// create connection
			conn = DriverManager.getConnection("jdbc:derby://localhost:1527/trining", "derby", "derby");
			System.out.println("*** DB Connection is successful .... ***\n");

			String query = "SELECT * FROM tbl_product WHERE PRICE >= ?";
			ps = conn.prepareStatement(query);

			System.out.println("Enter Product Price: ");
			double price = Double.parseDouble(s.nextLine());
			ps.setDouble(1, price);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				price = rs.getDouble(3);
				int qty = rs.getInt(4);
				System.out.println(id+"\t"+name+"\t"+price+"\t"+qty);
			}
			System.out.println("\nRecords successfully inserted....");

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();

		}
	}

}
