package com.abm.demo.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertData {

	public static void main(String[] args) {

		Connection conn = null;
		Statement stmt = null;
		try {
			// load driver
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			// create connection
			conn = DriverManager.getConnection("jdbc:derby://localhost:1527/trining", "derby", "derby");
			System.out.println("*** DB cConnection is successful .... ***\n");

			stmt = conn.createStatement();
			stmt.executeUpdate("INSERT INTO tbl_product VALUES (115,'Cloth', 4599.99 , 2)");
			System.out.println("Record successfully inserted....");

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();

		}
	}

}
