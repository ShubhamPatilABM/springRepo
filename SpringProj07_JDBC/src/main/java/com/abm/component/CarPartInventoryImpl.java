package com.abm.component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.abm.comp.interfaces.ICarPartInventory;
import com.abm.entity.CarPart;

@Component("carPartsInv")
public class CarPartInventoryImpl implements ICarPartInventory {

	public void addNewPArt(CarPart carPart) {
		Connection conn = null; 
		
		try {
			// load driver
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			// create connection
			conn = DriverManager.getConnection("jdbc:derby://localhost:1527/training", "derby", "derby");
			System.out.println("*** DB Connection is successful .... ***\n");

			String query = "INSERT INTO tbl_car_parts(PART_NAME, CAR_MODEL, PRICE, QUANTITY) VALUES (?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(query);

			ps.setString(1, carPart.getPartName());
			ps.setString(2, carPart.getCarModel());
			ps.setDouble(3, carPart.getPrice());
			ps.setInt(4, carPart.getQuantity());

			ps.executeUpdate();
			System.out.println("Record successfully inserted....");

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();   // bad approach, rather we should throw exception
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}

	public List<CarPart> getAvailableParts() {
		Connection conn = null;
		try {
			// load driver
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			// create connection
			conn = DriverManager.getConnection("jdbc:derby://localhost:1527/training", "derby", "derby");
			System.out.println("*** DB Connection is successful .... ***\n");

			String query = "SELECT * FROM tbl_car_parts";
			PreparedStatement ps = conn.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			
			List<CarPart> list = new ArrayList<>();
			
			while (rs.next()) {
				CarPart cp = new CarPart();				
				cp.setPartNo(rs.getInt("PART_NO"));
				cp.setPartName(rs.getString("PART_NAME"));
				cp.setCarModel(rs.getString("CAR_MODEL"));
				cp.setPrice(rs.getDouble("PRICE"));
				cp.setQuantity(rs.getInt("QUANTITY"));
				list.add(cp);
			}
			return list;

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();  // bad approach, rather we should throw exception
			return null;

		}finally {
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}

}
