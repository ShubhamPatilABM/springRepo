package com.abm.comp.interfaces;

import java.util.List;

import com.abm.entity.CarPart;

public interface ICarPartInventory {
	
	public void addNewPArt(CarPart carPart) ;
	
	public List<CarPart> getAvailableParts();

}
