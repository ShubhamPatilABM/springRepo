package com.abm.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.abm.component.TextEditor;

public class RunApp {
	
	public static void main(String[] args) {
		// loading the IOC container
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-webmvc.xml");
		
		// Accessing a bean
		TextEditor te = (TextEditor) ctx.getBean("txtEditor");
		te.loadDocument("abc.txt");
		
		
	}

}
