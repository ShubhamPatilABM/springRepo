package com.abm.component;

import org.springframework.stereotype.Component;

@Component("spelChecker")
public class SpelChecker {

	public void checkSpellingMistakes(String document) {
		System.out.println("spell checker executed for : " + document);

	}

}
