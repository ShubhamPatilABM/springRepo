package com.abm.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("txtEditor")
public class TextEditor {

	@Autowired  // DI
	private SpelChecker sp;

	public void loadDocument(String document) {
		System.out.println("text editor loaded : " + document);
		// SpelChecker sp = new SpelChecker();  >>>>>>> because of @Autowired
		sp.checkSpellingMistakes(document);

	}

}
