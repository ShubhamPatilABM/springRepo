package com.abm.examApp;

import java.util.List;

public class Question {

	private String question;
	private List<Option> option;

	public Question(String question) {
		this.question = question;
	}

	@Override
	public String toString() {
		return "Question [question=" + question + ", option=" + option + "]";
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<Option> getOption() {
		return option;
	}

	public void setOption(List<Option> option) {
		this.option = option;
	}

}
