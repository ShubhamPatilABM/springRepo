package com.abm.examApp;

import java.util.List;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		QuestionBankLoader qbl = new QuestionBankLoader();
		List<Question> questions = qbl.loadQuestionsOnJAva();

		System.out.println("Welcome to Pariksha....");

		int score = 0;
		for (Question que : questions) {
			System.out.println("Q. " + que.getQuestion());
			int index = 1;
			for (Option opt : que.getOption()) {
				System.out.println(index++ + ". " + opt.getOption());
			}
			System.out.println("Enter the right answer : ");
			int ans = s.nextInt();
			Option selectOption = que.getOption().get(ans - 1);
			if (selectOption.isRightAnswer())
				score++;
		}
		System.out.println("\n\nExam Over!!");
		System.out.println("Your score is " + score);
	}

}
