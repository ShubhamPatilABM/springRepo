package com.abm.examApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionBank {

	private Map<String, List<Question>> questionBank = new HashMap<>();

	public void addNewSubject(String subject) {
		questionBank.put(subject, new ArrayList<>()); //initially O questions
	}

	public void addNewQuestion(String subject, Question question) {
		List<Question> list = questionBank.get(subject); //get existing questions from subject wise
		list.add(question);  // add new question in question list
	}

	public List<Question> getQuestionsFor(String subject) {
		return questionBank.get(subject);
	}
}
