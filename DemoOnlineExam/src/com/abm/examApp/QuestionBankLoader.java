package com.abm.examApp;

import java.util.ArrayList;
import java.util.List;

public class QuestionBankLoader {

	public List<Question> loadQuestionsOnJAva() {
		QuestionBank qb = new QuestionBank();

		qb.addNewSubject("Java");

		// first Question
		Question q = new Question("How do you create a variable with the numeric value 5?");
		List<Option> ops = new ArrayList<>();
		ops.add(new Option("num x = 5", false));
		ops.add(new Option("int x = 5;", true));
		ops.add(new Option("float x = 5", false));
		ops.add(new Option("num x;", false));
		q.setOption(ops);
		qb.addNewQuestion("Java", q);

		// 2nd Question ****** using same RefVar *************
		q = new Question("Java is short for JavaScript?");
		ops = new ArrayList<>();
		ops.add(new Option("true", false));
		ops.add(new Option("false", true));
		q.setOption(ops);
		qb.addNewQuestion("Java", q);

		return qb.getQuestionsFor("Java");

	}
}
