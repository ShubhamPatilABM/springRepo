package om.abm.entity.one_to_many;

import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_song1")
public class Song {

	@Id //PK
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pid")
	private int sId;
	
	@Column(name = "song_name")	
	private String sName;
	
	@Column(name = "song_artist")
	private String artist;
	
	@Column(name = "song_duration")
	private LocalTime duration;
	
	@ManyToOne  // many songs in an album
	@JoinColumn(name = "al_id")  //FK
	private Album album;

	public int getsId() {
		return sId;
	}

	public void setsId(int sId) {
		this.sId = sId;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public LocalTime getDuration() {
		return duration;
	}

	public void setDuration(LocalTime duration) {
		this.duration = duration;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}
	
}
