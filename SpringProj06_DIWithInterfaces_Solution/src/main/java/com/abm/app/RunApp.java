package com.abm.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abm.comp.interfaces.IAtm;
import com.abm.component.HDFCAtmImpl;

public class RunApp {
	
	public static void main(String[] args) {
		// loading the IOC container
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-webmvc.xml");
		
		// Accessing a bean
		
		//HDFCAtmImpl hdfc =  (HDFCAtmImpl) ctx.getBean("hdfcAtm");
			//OR
		IAtm atm =   ctx.getBean(HDFCAtmImpl.class);		
		atm.withdraw(4455, 1000.00);
		
		
	}

}
