package com.abm.component;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.abm.comp.interfaces.IAtm;
import com.abm.comp.interfaces.IBank;

@Component("hdfcAtm")
public class HDFCAtmImpl implements IAtm {

	@Autowired
	private List<IBank> banks;

	public void withdraw(int accno, double amount) {
		System.out.println("User at HDFC ATM want to withdraw : " + amount + " from a/c : " + accno);
		IBank currentBank = null;
		for (IBank bank : banks) {
			if (bank.isCustomer(accno)) {
				currentBank = bank;
				break;
			}
		}
		currentBank.withdraw(1122, accno, amount);
	}

}
