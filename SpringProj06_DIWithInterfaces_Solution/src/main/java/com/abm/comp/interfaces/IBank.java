package com.abm.comp.interfaces;

public interface IBank {
	
	public boolean isCustomer(int accno);
	public void withdraw(int atmId, int accno, double amount);


}
