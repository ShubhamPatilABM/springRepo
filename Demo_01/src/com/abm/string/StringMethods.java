package com.abm.string;

public class StringMethods {
		
	public static void main(String[] args) {
		String s1 = "Hello";
		System.out.println(s1.hashCode());
		s1="Hi";
		System.out.println(s1.hashCode());
		//s1.toLowerCase();
		//s1 = "Hi"; // first string val be GC
		System.out.println(s1);
		
		System.out.println("====reverse the string=====");
		String str = new String("My name is Shubham");
			
		int l = str.length();
		
		for(int i=str.length()-1; i>=0;i--) {
			char ch = str.charAt(i);
			System.out.print(ch);
		}
		System.out.println("\n====Index of character ");		
		int a = str.indexOf('M');
		System.out.println(a);
		
		String date = String.join("/", "13", "Oct", "2022");
		System.out.println("Today's date is: "+date);
		
		String s22 = String.valueOf(true);
		System.out.println(s22);
	}

}
