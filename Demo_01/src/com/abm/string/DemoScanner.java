package com.abm.string;

import java.util.Scanner;

public class DemoScanner {
	
	public static void add(int x, int y) {
		System.out.println("Sum of Numbers "+x+" "+y+" is :"+(x+y));		
	}
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter 1st No:");
		int x=s.nextInt();
		System.out.println(x);
		System.out.println("Enter 2nd No:");
		int y=s.nextInt();	
		add(x, y);
	}


}
