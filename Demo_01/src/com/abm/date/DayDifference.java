package com.abm.date;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class DayDifference {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Please Enter Date of Joining");
		int dd = s.nextInt();
		System.out.println("Please Enter Month of Joining");
		int mm = s.nextInt();
		System.out.println("Please Enter Year of Joining");
		int yy = s.nextInt();
		s.close();
		
		LocalDate jdt = LocalDate.of(yy, mm, dd);
		LocalDate cdt = LocalDate.now();

		System.out.println("========== Using Period ==========");
		Period pd = Period.between(jdt, cdt);
		int yrs = pd.getYears();
		int mth = pd.getMonths();
		int days = pd.getDays();
		System.out.println("**** Experience Till Date is ***");
		System.out.println("Years: " + yrs + " Months :" + mth + " Days : " + days);

	}
}
