package com.abm.date;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class WordTime {
	public static void main(String[] args) {
		
		System.out.println("====== **************** Older approach **************** =======");
		Calendar cNY = Calendar.getInstance();
		cNY.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		System.out.print("Cuurent Time in America/New_York is : ");
		System.out.println(cNY.get(Calendar.HOUR_OF_DAY)+":"+cNY.get(Calendar.MINUTE)+":"+cNY.get(Calendar.SECOND));
		
		TimeZone tz = TimeZone.getTimeZone("America/New_York");
		GregorianCalendar gc = new GregorianCalendar();	
		
		
		System.out.println("====== **************** Java8 approach **************** =======");		
		ZoneId zone = ZoneId.of("America/New_York");
		ZoneId zoneInd = ZoneId.of("Asia/Kolkata");
		
		LocalDateTime ldt = LocalDateTime.now(zone);
		LocalDateTime ldtInd = LocalDateTime.now(zoneInd);
		

		// Period period = Period.between(ldt, ldtInd); >> not for LocalDateTime
				 
		Duration dur = Duration.between(ldt, ldtInd);
		//long hours = dur.toHours();
		//long minutes = dur.toMinutes();
		//long sec = dur.getSeconds();
		
		System.out.println("Cuurent Sate in America/New_York is : "+ldt.getDayOfMonth()+"/"+ldt.getMonth()+"/"+ldt.getYear());
		System.out.println("Cuurent Time in America/New_York is : "+ldt.getHour()+":"+ldt.getMinute()+":"+ldt.getSecond());
		
		System.out.println("Cuurent Sate in Asia/Kolkata is : "+ldtInd.getDayOfMonth()+"/"+ldtInd.getMonth()+"/"+ldtInd.getYear());
		System.out.println("Cuurent Time in Asia/Kolkata is : "+ldtInd.getHour()+":"+ldtInd.getMinute()+":"+ldtInd.getSecond());

		System.out.println("Time Differrence between America/New_York and Asia/Kolkata is :"+dur);

	}

}

