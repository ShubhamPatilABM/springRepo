package com.abm.date;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateFormat {

	public static void main(String[] args) {
		
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss:ms");
		DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
		DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("yyyy/dd/MM HH:mm:ss");
		String dateTime = dtf.format(now);
		String dateTime2 = dtf2.format(now);
		String dateTime3 = dtf3.format(now);
		System.out.println("Current Date and Time is : " + dateTime);
		System.out.println("Current Date and Time is : " + dateTime2);
		System.out.println("Current Date and Time is : " + dateTime3);
	}
}
