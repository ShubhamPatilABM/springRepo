package com.abm.demo;

public class Arrays {

	public static void main(String[] args) {

		int[] arr = new int[5];
		arr[0] = 10;
		arr[1] = 20;
		arr[2] = 30;
		arr[3] = 40;
		arr[4] = 50;

		int[] arr2 = { 100, 200, 300, 400, 500, 600 };

		System.out.println("==========using for loop=======");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + "\t");
		}
		System.out.println("\n==========using Enhanced/Extended for loop=======");
		for (int i : arr2) {
			System.out.print(i + "\t");

		}
	}

}
