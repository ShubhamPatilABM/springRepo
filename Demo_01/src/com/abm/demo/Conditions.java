package com.abm.demo;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Conditions {

	public static void main(String[] args) {

		System.out.println("====If Condition====");
		LocalDateTime ldt = LocalDateTime.now();
		if (ldt.getHour() < 12)
			System.out.println("***Good Morning SP***");
		else if (ldt.getHour() < 16)
			System.out.println("***Good Afternoon SP***");
		else if (ldt.getHour() < 21)
			System.out.println("***Good Evening SP***");
		else
			System.out.println("***Good Night SP***");

		System.out.println("====switch case ====");
		LocalDate dt = LocalDate.now();
		DayOfWeek day = dt.getDayOfWeek();
		switch (day) {
		case MONDAY:
			System.out.println("Today is Monday");
			break;
		case TUESDAY:
			System.out.println("Today is Tuesday");
			break;
		case WEDNESDAY:
			System.out.println("Today is Wednesday");
			break;
		case THURSDAY:
			System.out.println("Today is Thursday");
			break;
		case FRIDAY:
			System.out.println("Today is Friday");
			break;
		case SATURDAY:
			System.out.println("Today is Saturday");
			break;
		default:
			System.out.println("Today is Sunday");
		}

	}

}
