package com.abm.demo;

public class LoopsAndConditions {
	public static void main(String[] args) {
		System.out.println("***For Loop***");
		for (int i = 1; i <=5; i++) {
			System.out.println(i);
			
		}
		
		System.out.println("***While Loop***");
		int j=1;
		while (j<=5) {
			System.out.println(j);
			j++;			
		}
		
		
		System.out.println("***Do While Loop***");
		int k=1;
		do {
			System.out.println(k);
			k++;			
		}while (k<=5);
		
		
		System.out.println("***For Loop***");
		for (int i1 = 6; i1 <=5; i1++) {
			System.out.println(i1);
			
		}
		
		System.out.println("***While Loop***");
		int j1=6;
		while (j1<=5) {
			System.out.println(j1);
			j1++;			
		}
		
		System.out.println("***Do While Loop***");
		int k1=6;
		do {
			System.out.println(k1);
			k1++;			
		}while (k<=5);
	}

}
