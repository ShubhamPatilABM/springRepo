package com.abm.training;

public class PersonTest {

	public static void main(String[] args) {
		
		System.out.println(Person.population);\
		
		Person p1 = new Person();
		p1.setName("John");
		p1.setAge(25);
		
		System.out.println(Person.population);
		Person p2 = new Person();
		p2.setName("Alen");
		p2.setAge(30);

		System.out.println("Person 1 :" + p1.getName() + " " + p1.getAge());
		System.out.println("Person 2 :" + p2.getName() + " " + p2.getAge());
		System.out.println(Person.population);

	}
}
