package com.abm.training;

public class Customer2 {
	int id;
	String name, email;
	long moNo;

	// parameterized constructor
	public Customer2(int id, String name, String email, long moNo) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.moNo = moNo;
		// System.out.println("Customer Id: " + cs.id);
		System.out.println("Customer Id: " + this.id);
		System.out.println("Customer Name :" + this.name);
		System.out.println("Customer Email:" + this.email);
		System.out.println("Customer Mobile No:" + this.moNo);
		System.out.println("*****************Constructor*************************************");
	}
	
	void display() {
		System.out.println("Customer Id: " + this.id);
		System.out.println("Customer Name :" + this.name);
		System.out.println("Customer Email:" + this.email);
		System.out.println("Customer Mobile No:" + this.moNo);
		System.out.println("**********************display()*********************************");
	}
	public static void main(String[] args) {
		Customer2 cs = new Customer2(101, "Chetan", "c@gmail.com", 98878789898L);
		cs.display();
		Customer2 cs1 = new Customer2(102, "Anuja", "a@gmail.com", 9898989977L);
		cs1.display();
		Customer2 cs2 = new Customer2(103, "Pritee", "p@gmail.com", 9898976548L);
		cs2.display();
		Customer2 cs3 = new Customer2(104, "Vikalp", "v@gmail.com", 98989844448L);
		//cs3.display();
		Customer2 cs4 = new Customer2(105, "Arif", "ar@gmail.com", 9898989888L);
		//cs4.display();
		Customer2 cs5 = new Customer2(106, "Pritam", "pr@gmail.com", 989800098L);
		//cs5.display();
		Customer2 cs6 = new Customer2(107, "Nitin", "n@gmail.com", 9898989978L);
		//cs6.display();
		
		/*cs.id = 101;
		cs.name = "Chetan";
		cs.email = "c@gmail.com";
		cs.moNo = 9898989898L;*/

		/*System.out.println("Customer Id: " + cs.id);
		System.out.println("Customer Name :" + cs.name);
		System.out.println("Customer Email:" + cs.email);
		System.out.println("Customer Mobile No:" + cs.moNo);*/
	}

}
