package com.abm.training;

public class Customer3 {
	//private int id;
	int id;
	String name, email;
	long moNo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getMoNo() {
		return moNo;
	}
	public void setMoNo(long moNo) {
		this.moNo = moNo;
	}
	
	void display() {
		System.out.println("**********************display()*********************************");
		System.out.println("Customer Id: " + this.id);
		System.out.println("Customer Name :" + this.name);
		System.out.println("Customer Email:" + this.email);
		System.out.println("Customer Mobile No:" + this.moNo);
	}
	public static void main(String[] args) {
		Customer3 cs = new Customer3();
		cs.setId(101);
		cs.setName("Vinayak");
		cs.setEmail("v@gmail.com");
		cs.setMoNo(9898989898l);
		cs.display();

		Customer3 cs1 = new Customer3();
		cs1.setId(102);
		cs1.setName("Vina");
		cs1.setEmail("v@gmail.com");
		cs1.setMoNo(9899999898l);
		cs1.display();
			
	}

}
