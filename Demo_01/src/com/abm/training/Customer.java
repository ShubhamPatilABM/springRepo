package com.abm.training;

public class Customer {
	static int id;
	String name, email;
	long moNo;

	public static void main(String[] args) {
		
		Customer cs = new Customer();
		id = 101;
		cs.name = "Chetan";
		cs.email = "c@gmail.com";
		cs.moNo = 9898989898L;

		System.out.println("Customer Id: " + cs.id);
		System.out.println("Customer Name :" + cs.name);
		System.out.println("Customer Email:" + cs.email);
		System.out.println("Customer Mobile No:" + cs.moNo);
	}

}
