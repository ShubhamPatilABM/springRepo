package om.abm.entity.bi_association;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_person")
public class Person {

	@Id //PK
	@GeneratedValue 
	@Column(name = "pid")
	private int pid;
	
	@Column(name = "p_name")
	private String pName;
	
	@Column(name = "DOB")
	private LocalDate dob;

	@Column(name = "mobno")
	private long mobno;
	
	// Bidirectional Association
	@OneToOne(cascade = CascadeType.ALL)   // one to one Association or Has-A-Relation
	@JoinColumn(name = "passport_no") 
	private Passport passport;
	
	public Passport getPassport() {
		return passport;
	}

	public void setPassport(Passport passport) {
		this.passport = passport;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public long getMobno() {
		return mobno;
	}

	public void setMobno(long mobno) {
		this.mobno = mobno;
	}
	
	
	
	


}
