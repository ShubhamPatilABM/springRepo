package om.abm.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import om.abm.entity.bi_association.Passport;

public class PassportDao {

	// read data from persistence.xml
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");
	EntityManager em = emf.createEntityManager();
	Passport passport = null;

	public void store(Passport passport) {
		EntityTransaction tx = em.getTransaction();
		tx.begin(); // transaction began
		em.persist(passport); // persist() method generates insert query
		tx.commit();
		em.close();
		emf.close();
	}

	public Passport fetch(int id) {
		Passport address = em.find(Passport.class, id);
		em.close();
		emf.close();
		return address;
	}

}
