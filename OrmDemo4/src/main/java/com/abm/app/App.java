package com.abm.app;

import java.time.LocalDate;
import java.util.Scanner;

import om.abm.dao.PassportDao;
import om.abm.dao.PersonDao;
import om.abm.entity.bi_association.Passport;
import om.abm.entity.bi_association.Person;

public class App {

	public static void main(String[] args) {

		Person person = new Person();
		Passport passport = new Passport();

		PersonDao perDao = new PersonDao();
		
		person.setpName("Pritam");
		person.setDob(LocalDate.of(1998, 1, 30));
		person.setMobno(787878898l);
		
		person.setPassport(passport);
		

		passport.setIssueDate(LocalDate.of(2020, 8, 17));
		passport.setExpDate(LocalDate.of(2030, 8, 17));
		passport.setIssuedBy("Govt of India");
		
		perDao.store(person);

		System.out.println("Record Inserted successfuly...");

	}

}
