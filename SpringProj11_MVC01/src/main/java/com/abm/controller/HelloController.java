package com.abm.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {
	
	@RequestMapping("/hello")
	public String sayHello(Map model) {  // Best
		
		model.put("message", "Welcome to Spring MVC");
		return "hello.jsp";
	}
	
	// OR
	@RequestMapping("/hello2")
	public ModelAndView sayHello() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("message", "Welcome to Spring MVC");
		mv.setViewName("hello.jsp");
		return mv;
	}

}
