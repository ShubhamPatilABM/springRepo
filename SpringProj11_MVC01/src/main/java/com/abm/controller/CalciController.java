package com.abm.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CalciController {
	
	@RequestMapping("/calculator")
	public String calculat(@RequestParam ("no1") int no1, @RequestParam ("no2") int no2, Map<String, Object> model) {  // Best
		int result = no1 + no2;
		model.put("n1", no1);
		model.put("n2", no2);
		model.put("result", result);	
		return "calci.jsp";
	}
}
