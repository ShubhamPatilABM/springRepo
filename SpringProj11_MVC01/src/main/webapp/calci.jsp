<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	
	<form action="calculator" method="post" >
		Enter 1st No: <input name="no1" value="${n1}"> </br></br>
		Enter 2nd No: <input name="no2" value="${n2}"> </br></br>
		<c:choose>
			<c:when test="${result != null}">
				<button type="submit" disabled>Add</button>
			</c:when>
			<c:otherwise>
				<button type="submit">Add</button>
			</c:otherwise>
		</c:choose>
	</form>
<h3>Result is : ${result}</h3>
</body>
</html>