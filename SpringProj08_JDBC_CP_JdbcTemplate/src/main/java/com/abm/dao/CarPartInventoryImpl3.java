package com.abm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.abm.dao.interfaces.ICarPartInventory;
import com.abm.entity.CarPart;

@Component("carPartsInv3")
public class CarPartInventoryImpl3 implements ICarPartInventory {

	// DataSource == Connection Pool
	@Autowired
	private DataSource dataSource;

	public void addNewPArt(CarPart carPart) {

		// JdbcTemplate from Spring jdbc
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		String query = "INSERT INTO tbl_car_parts(PART_NAME, CAR_MODEL, PRICE, QUANTITY) VALUES (?,?,?,?)";
		jt.update(query, carPart.getPartName(), carPart.getCarModel(), carPart.getPrice(), carPart.getQuantity());
		System.out.println("Record successfully inserted....");
	}

	public List<CarPart> getAvailableParts() {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		String query = "SELECT * FROM tbl_car_parts";
		List<CarPart> list = jt.query(query, BeanPropertyRowMapper.newInstance(CarPart.class));
		return list;
		
	}

}
