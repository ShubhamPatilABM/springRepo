package com.abm.demo;

public class Person {

	String name;
	int age;

	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}
	/*
	 * public Person(String pname, int page) { name = pname; age = page; }
	 */

	@Override  //called internally by Object class
	public String toString() { 
		return "Person [name=" + name + ", age=" + age + "]";
	}

	public static void main(String[] args) {

		Person p1 = new Person("Ankush", 22);
		Person p2 = new Person("Anku", 22);
		System.out.println("Name :" + p1.name + "\nAge : " + p1.age);
		System.out.println(p1);
		System.out.println(p2);
	}

}
